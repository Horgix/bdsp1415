#! /usr/bin/env make

PROJECT		= versionning
TEAM		= gconfs
HIERARCHY	= /gconfs/
SOURCES		= src/content.md
OUTPUT		= ${PROJECT}_${TEAM}
IMAGES		=

all:: report

report:: logos
	pandoc ${SOURCES} \
	  -t beamer \
	  -V theme:Custom \
	  --slide-level 2 \
	  -o ${OUTPUT}.pdf

logos: logo_epita.png logo_gconfs.png logo_gconfs_mini.png

images: ${IMAGES}

logo_%.png:
	@if ! [ -d 'images/' ]; then mkdir 'images'; fi
	@if ! [ -e 'images/$@' ]; then \
	  wget 'http://horgix.fr/epita/logos/$@'; \
	  mv $@ images/; \
	fi

%.png:
	@if ! [ -d 'images/' ]; then mkdir 'images'; fi
	@if ! [ -e 'images/$@' ]; then \
	  wget 'http://horgix.fr/epita/${PROJECT}/images/$@'; \
	  mv $@ images/; \
	fi

upload::
	scp ${OUTPUT}.* horgix@horgix.fr:/www/http/${HIERARCHY}

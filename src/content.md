% Versionning & Git
% Alexis  *Horgix*  Chotard
  alexis.horgix.chotard@gmail.com
% Vendredi 11 décembre 2015

# Versionning

## Why use it

* Manage your source code (SCM)
* Keep a trace of every modification
* Come back to any previous state
* Collaborate

# VCS types

## cpold

    horgix@avalon ~/cpold$ ls
    file                    file.OK       file.old.old
    file.2013-08-12         file.old      file.old.test
    file.2013-11-15.ok      file.OLD      file.horgix
    file.to-check           file.backup   file.old.not-working

## Local

![local](images/local.png)

## Remote/collaboration

Vocabulary:

* clone
* commit
* push
* pull

## Centralized

![centralized](images/centralized.png)

## Distributed

![distributed](images/distributed.png)

## Some Version Control Systems

* Git
* Subversion (SVN)
* Mercurial
* GNU Bazaar
* CVS
* Perforce

# Git

## Where it comes from

* Linus Torvald
* Linux kernel maintenance
* 1991 - 2002 : patches
* 2002 - 2005 : BitKeeper
* 2005 - Now  : Git

## "Git, c'est le système de rendu d'Epita"

* Google (https://github.com/google)
* Facebook (https://github.com/facebook)
* Microsoft (http://aspnetwebstack.codeplex.com/)
* Twitter (https://github.com/twitter)
* Perl (http://perl5.git.perl.org/perl.git)
* Android (https://android-review.googlesource.com)
* Git (https://git.kernel.org/cgit/git/git.git/)
* ...

G(ot) it ?

## Theory

![lifecycle](images/lifecycle.png)

## Practice

* clone
* add
* commit
* pull
* push

# Demo

## Initialization

```
$ git init
Initialized empty Git repository
in /home/horgix/demogit/.git/
```

## Status

```
$ git status
On branch master

Initial commit

nothing to commit (create/copy files
and use "git add" to track)
```

## File creation

```
$ echo 'a' > tstfile.txt
$ git status
On branch master
Initial commit
Untracked files:
  (use "git add <file>..." to include
  in what will be committed)
  tstfile.txt

nothing added to commit but untracked
files present (use "git add" to track)
```

## Add file

```
$ git add tstfile.txt
$ git status
On branch master

Initial commit

Changes to be committed:
  (use "git rm --cached <file>..."
  to unstage)

  new file:   tstfile.txt
```

## Commit

```
$ git commit -m "Add tstfile for demo"
[master (root-commit) c424e84]
Add tstfile for demo
 1 file changed, 1 insertion(+)
 create mode 100644 tstfile.txt
```
```
$ git status
On branch master
nothing to commit, working directory clean
```

## Logs

```
$ git log
commit c424e84e19333d92a744ef1d6b999363b2f640a5
Author: Alexis 'Horgix' Chotard <alexis.horgix.chotard@gmail.com>
Date:   Fri Dec 11 20:25:56 2015 +0100

    Add tstfile for demo
```

## Good behaviors

"Commit early, commit often"

Only commit compiling code.

Write explicit commit messages.

Don't add :

* Binaries (.exe, .o, .out, .pdf, ...)
* System files (Thumbs.db, ...)
* Useless files (git is not a file sharing system)

# Where can I host my git repository

## GitHub - https://github.com/

* 5M users
* Unlimited contributors
* Web pages hosting (GitHub Pages)

## Bitbucket - https://bitbucket.org/

* 1M users
* Unlimited free repositories
* External authentication (Facebook, OpenID, Google)
* Also supports SVN and Mercurial

## Your own server

* git init

# Useful stuff

## \*Click click\*

* Sourcetree : http://www.sourcetreeapp.com
* Github GUI : https://windows.github.com
* Visual Studio tools for Git extension

## Random links

* Bible : http://git-scm.com/
* Test. : https://acu.epita.fr/git/

# Questions ?

